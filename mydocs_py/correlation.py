

def correlation(a , b ,unwanted='*'):
	a = list(a.lower())
	b = list(b.lower())
	
	count = 0 
	flag =0
	for i , j in zip(a,b) :
		if (not flag) and j not in unwanted:
			flag =1
		if i == j or j in unwanted :
			count = count + 1
			continue

		return 0
	return count >= len(a)/2 and flag
	

def correct_unwanted_char(arr):
    badWords = ["fuck" ,"cock" ,"suck" ,"piss" ,"bullshit" ,"ass" ,"asshole" ,"dick","shit" ,"motherfuck"]
    for k in range(len(arr)) :
        tmp = arr[k].split()
        flag = 0
        for j in badWords:
            for i in range(len(tmp)):
                if correlation(j ,tmp[i]) :
                    tmp[i]= j
                    flag =1
                    
        if flag :
            arr[k] = " ".join(tmp)
    return arr
print(correlation("fuck" ,"$%ck","!@#$%^&*()"))