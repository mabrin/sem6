
CREATE TABLE paper_auth(
autId int NOT NULL,
PID int NOT NULL ,
FOREIGN KEY (PID) REFERENCES PAPER(PID) ,
FOREIGN KEY (autId) REFERENCES author(autId)
);

INSERT INTO author (auth_first_name ,auth_last_name)
VALUES ("M."," Rudolph");

alter table paper auto_increment=0;

alter table paper add column corresponding_author int not null after conference;

ALTER TABLE Orders
ADD CONSTRAINT FK_PersonOrder
FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);


update paper set corresponding_author=4 where paper_id=1;

## where clause all statements
select * from instructor where salary > all(select salary from instructor where dept_name='Comp.
sci.' union select salary from instructor where dept_name='Biology' union select salary
from instructor where dept_name='finance');

SELECT avg(salary) ,dept_name FROM instructor group by dept_name desc limit 1;
SELECT * FROM department group by dept_name order by budget desc limit 1;

Show full tables ; ## see tables which base / view tables

change password in file  "change root password.pdf";

mysqldump -u root -p -A > backup
mysql -u root -p –all-databases < backup.sql 
# first create database university
mysql -u root -p university < create_university.sql

SHOW GRANTS;
Show grants for username ;
CREATE ROLE journalist;

GRANT SHOW DATABASES ON *.* TO journalist;
GRANT journalist to hulda;

# pattern matching
select id , name from paper_names where name like '%bayesian%';


create view author_sub as ($query)

source fb.sql ;

SHOW VARIABLES WHERE VARIABLE_NAME = 'event_scheduler' ;

### system variables 

SET GLOBAL event_scheduler = ON ;
set sql_mode='STRICT_ALL_TABLES' ; # meaning that overflow of input will result in fail of transaction instead of give warning
set sql_mode='EMPTY_STRING_IS_NULL'; ## binds empty string to null

# what is role ???
# A role bundles a number of privileges together. It assists larger organizations where,
# typically, a number of users would have the same privileges, and, previously, 
# the only way to change the privileges for a group of users was by changing each user's 
# privileges individually.


#create users
create user "coordinator"@"%" identified by "abc" ;
