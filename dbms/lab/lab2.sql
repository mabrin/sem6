CREATE DATABASE expert_system_VISHAL_KUMAR ;

USE expert_system_VISHAL_KUMAR ;


CREATE TABLE PAPER(
	PID int NOT NULL AUTO_INCREMENT,
	paper_name char(200),
	conference char(200),
	CID char(50),
	PRIMARY KEY (PID)
FOREIGN KEY (CID ) REFERENCES author(autId) ON


);


CREATE TABLE YEAR(

	year int ,
	PID INT ,
	FOREIGN KEY (PID) REFERENCES PAPER(PID) ON DELETE CASCADE,
	PRIMARY KEY (PID)

);

CREATE TABLE author(

	autId INT NOT NULL AUTO_INCREMENT ,
	auth_first_name char(200) NOT NULL,
	auth_last_name char(200) NOT NULL,
	PRIMARY KEY (autId)
);

-- CREATE TABLE corresponing_aut(
-- 	autId int NOT NULL,
-- 	PID int NOT NULL,
-- 	FOREIGN KEY (autId) REFERENCES author(autId),
-- 	FOREIGN KEY (PID) REFERENCES PAPER(PID) ,
-- 	PRIMARY KEY (autId)
-- );

CREATE  TABLE paper_auth(
	autId int NOT NULL,
	PID int NOT NULL ,
	FOREIGN KEY (PID) REFERENCES PAPER(PID) ON DELETE CASCADE ,
	FOREIGN KEY (autId) REFERENCES author(autId) ON DELETE CASCADE

);

INSERT INTO PAPER (paper_name , conference)
VALUES ("​ Structured embedding models for grouped data" ,"Neural Information Processing Systems") ,
("Topographic factor analysis: A Bayesian model for inferring brain networks fromneural data","PLoS ONE"),
		("Nonconvex finite-sum optimization via SCSG methods" ,NULL),
		("Nonparametric combinatorial sequence models" ,"15th Annual International Conference on Research in Computational Molecular Biology (RECOMB) ​ , Vancouver, BC"),
		("15th Annual International Conference on Researchin Computational Molecular Biology (RECOMB) ​ , Vancouver, BC",NULL) ;

INSERT INTO author (auth_first_name ,auth_last_name)
	VALUES  ("M."," Rudolph"),	("F. ","Ruiz") ,	("S."," Athey "),	("*D. ","Blei"),	("K. ","Norman") ,	("R. ","Ranganath"),	("J."," Manning"),	("L. ","Lei"),	("C. ","Ju"),	("J.","Chen"),	("M. I."," Jordan"),	("F."," Wauthier"),	("N. ","Jojic"),	("Sahely ","Bhadra"),	("Samuel"," Kaski"),	("Juho"," Rousu");

INSERT INTO author (auth_first_name , auth_last_name)
	VALUES ("S.","Vishwanathan");

INSERT INTO paper_auth (autId ,PID)
	VALUES  (1,1),(2,1),(3,1),(4,1),(4,2),(5,2),(6,2),(7,2);

INSERT INTO paper_auth (autId ,PID)
	VALUES  (8,3),(9,3),(10,3),(11,3),(12,4),(13,4),(11,4),(14,5),(15,5),(16,5);

INSERT INTO YEAR (PID ,year) VALUES (1,2017),(2,2014),(3,2018),(5,2017);

UPDATE PAPER SET CID =4 WHERE PID=1 LIMIT 1;
UPDATE PAPER SET CID =4 WHERE PID=2 LIMIT 1;
UPDATE PAPER SET CID =17 WHERE PID=3 LIMIT 1;
UPDATE PAPER SET CID =11 WHERE PID=4 LIMIT 1;
UPDATE PAPER SET CID =14 WHERE PID=5 LIMIT 1;

DELETE FROM PAPER WHERE PID=4 LIMIT 1;
delete from paper_auth where PID =5 ;

INSERT INTO author (auth_first_name , auth_last_name)
VALUES ("Antti"," Kangasraasio"),
("Kumaripaba ","Athukorala"),
("Andrew ","Howes"),
("Jukka ","Corander"),
("Antti ","Oulasvirta"),
("B.","Milch"),
("B.","Marthi"),
("S.","Russell"),
("D.","Sontag"),
("D. L.","Ong"),
("A.","Kolobov"),
("Jonathan T.","Barron"),
("David W.","Hogg"),
("Dustin","Lang1"),
("Sam","Roweis");

INSERT INTO PAPER (paper_name , conference ,CID) VALUES
("Inferring Cognitive Models from Data using Approximate Bayesian Computation","ACM SIGCHI annual conference on human factors in computing systems, Proceedings of the 2017 CHI Conference on Human Factors in Computing Systems",15),
("BLOG: Probabilistic Models with Unknown Objects","Statistical Relational Learning",26),
("Blind Date: Using Proper Motions to Determine the Ages of Historical Images","The Astronomical Journal 136",32);

INSERT INTO YEAR (PID ,year) VALUES (6,2017),(7,2007),(8,2008);
INSERT INTO paper_auth (autId ,PID)
	VALUES  (19,6),(20,6),(21,6),(22,6),(15,6),(23,7),(24,7),(25,7),(26,7),(27,7),(28,7),(29,8),(20,8),(31,8),(32,8),(33,9),(15,9);

DELETE from author where auth_first_name="Antti" and auth_last_name=" Kangasraasio" LIMIT 1;

INSERT INTO author (auth_first_name , auth_last_name)
VALUES ("Pekka","Parviainen");

INSERT INTO PAPER (paper_name , conference ,CID) VALUES
("Learning structures of Bayesian networks for variable groups","INTERNATIONAL JOURNAL OF APPROXIMATE REASONING, 88:110-127",15);



select p.paper_name , y.YEAR from PAPER as p ,year as y where p.PID=y.PID AND y.year=2017;


select p.paper_name ,a.auth_first_name from PAPER as p ,author as a, paper_auth as pa where p.PID=pa.PID and a.autID=pa.autId AND a.auth_first_name="Samuel" and a.auth_last_name=" Kaski"  ;


 select p.paper_name , p.PID from PAPER AS p ,paper_auth as pa where p.PID =pa.PID  group by p.PID HAVING COUNT(p.PID)>2;
--8
SELECT p.paper_name FROM paper as p ,paper_auth as pa ,author as a WHERE a.auth_first_name REGEXP '[*Sam*]' AND p.PID=pa.PID AND pa.auth_id=a.authId ;

--9
 SELECT  p.paper_name FROM PAPER as p ,paper_auth as pa ,author as a WHERE a.auth_first_name like  '%Sam%' AND p.PID=pa.PID AND pa.autId=a.autId ;


--10
 DELETE FROM paper_auth      where paper_auth.autID=15  ;
 DELETE FROM author      where auth_first_name="Samuel" and auth_last_name=" Kaski" limit 1 ;

--11
DROP TABLE YEAR ;







INSERT INTO paper_auth (autId ,PID) VALUES  (15,5), (15,9);


INSERT INTO author (autId ,auth_first_name , auth_last_name)
VALUES (15,"Samuel"," Kaski");







