#/var/lib/mysql
#/etc/mysql
#

### Variables
show [session | global] variables like 'autocommit';
set session var = val;
set global var = val;

SET FOREIGN_KEY_CHECKS=0;
SET FOREIGN_KEY_CHECKS=1;




##########################################################################################
# import | export
##########################################################################################

mysqldump -u [username] –p[password] [database_name] > [dump_file.sql]

#### from CSV -> table
	
	# #col and type of col should be same
	LOAD DATA INFILE 'c:/tmp/discounts.csv' 
	INTO TABLE discounts 
	FIELDS TERMINATED BY ',' 
	ENCLOSED BY '"'
	LINES TERMINATED BY '\n'
	IGNORE 1 ROWS;

	# import with conversion
	LOAD DATA INFILE 'c:/tmp/discounts_2.csv'
	INTO TABLE discounts
	FIELDS TERMINATED BY ',' ENCLOSED BY '"'
	LINES TERMINATED BY '\n'
	IGNORE 1 ROWS
	(title,@expired_date,amount)
	SET expired_date = STR_TO_DATE(@expired_date, '%m/%d/%Y');


##### from Table -> CSV
	
	SELECT 
	    orderNumber, status, orderDate, requiredDate, comments
	FROM
	    orders
	WHERE
	    status = 'Cancelled' 
	INTO OUTFILE 'C:/tmp/cancelled_orders.csv' 
	FIELDS ENCLOSED BY '"' 
	TERMINATED BY ';' 
	ESCAPED BY '"' 
	LINES TERMINATED BY '\r\n';


##########################################################################################
# transaction
##########################################################################################

there are some SQL statements, mostly data definition statements,
that you cannot use within a transaction:

	CREATE / ALTER / DROP DATABASE
	CREATE /ALTER / DROP / RENAME / TRUNCATE TABLE
	CREATE / DROP INDEX
	CREATE / DROP EVENT
	CREATE / DROP FUNCTION
	CREATE / DROP PROCEDURE



##########################################################################################
# foreign key
##########################################################################################

# To find the tables that will be effected due to cascading
USE information_schema;
 
SELECT 
    table_name
FROM
    referential_constraints
WHERE
    constraint_schema = 'database_name'
        AND referenced_table_name = 'parent_table'
        AND delete_rule = 'CASCADE'




##########################################################################################
#  procedure
##########################################################################################

SHOW PROCEDURE STATUS [LIKE 'pattern' | WHERE expr];
SHOW PROCEDURE STATUS;
SHOW PROCEDURE STATUS WHERE db = 'classicmodels';
SHOW PROCEDURE STATUS WHERE name LIKE '%product%';
SHOW CREATE PROCEDURE stored_procedure_name;



can be called from Procedures, triggers, applications
recursibe procedure - not all supports

Benifits : 
	stored procedures are compiled and stored in the database, so it is much faster than query
	Users sends 'call procname', such a small query
	interface to database
	administrator can give permissions


DECLARE x, y INT DEFAULT 0;
SET total_count = 10;
SELECT COUNT(*) INTO total_products

A variable that begins with the @ sign is session variable. It is available and accessible until the session ends.

call procName(in, @out);



####### IF-ELSEIF-ELSE
	
	IF expression THEN
	   statements;
	ELSEIF elseif-expression THEN
	   elseif-statements;
	...
	ELSE
	   else-statements;
	END IF;


###### Exception

DELIMITER $$
 
CREATE PROCEDURE insert_article_tags(IN article_id INT, IN tag_id INT)
BEGIN
 
 DECLARE CONTINUE HANDLER FOR 1062
 SELECT CONCAT('duplicate keys (',article_id,',',tag_id,') found') AS msg;
 
 -- insert a new record into article_tags
 INSERT INTO article_tags(article_id,tag_id)
 VALUES(article_id,tag_id);
 
 -- return tag count for the article
 SELECT COUNT(*) FROM article_tags;
END

####### exception

DELIMITER $$
 
CREATE PROCEDURE insert_article_tags_2(IN article_id INT, IN tag_id INT)
BEGIN
 
 DECLARE EXIT HANDLER FOR SQLEXCEPTION 
 SELECT 'SQLException invoked';
 
 DECLARE EXIT HANDLER FOR 1062 
        SELECT 'MySQL error code 1062 invoked';
 
 DECLARE EXIT HANDLER FOR SQLSTATE '23000'
 SELECT 'SQLSTATE 23000 invoked';
 
 -- insert a new record into article_tags
 INSERT INTO article_tags(article_id,tag_id)
   VALUES(article_id,tag_id);
 
 -- return tag count for the article
 SELECT COUNT(*) FROM article_tags;
END


###########  Custom Exceptions Names

DECLARE table_not_found CONDITION for 1051;
DECLARE EXIT HANDLER FOR  table_not_found SELECT 'Please create table abc first';
SELECT * FROM abc;


############## 

DELIMITER $$
 
CREATE PROCEDURE AddOrderItem(
          in orderNo int,
 in productCode varchar(45),
 in qty int, 
                         in price double, 
                         in lineNo int )
BEGIN
 DECLARE C INT;
 
 SELECT COUNT(orderNumber) INTO C
 FROM orders 
 WHERE orderNumber = orderNo;
 
 -- check if orderNumber exists
 IF(C != 1) THEN 
 SIGNAL SQLSTATE '45000'
 SET MESSAGE_TEXT = 'Order No not found in orders table';
 END IF;
 -- more code below
 -- ...
END


###### Nice Example
	DELIMITER $$
	 
	CREATE PROCEDURE GetCustomerLevel(
	    in  p_customerNumber int(11), 
	    out p_customerLevel  varchar(10))
	BEGIN
	    DECLARE creditlim double;
	 
	    SELECT creditlimit INTO creditlim
	    FROM customers
	    WHERE customerNumber = p_customerNumber;
	 
	    IF creditlim > 50000 THEN
	 SET p_customerLevel = 'PLATINUM';
	    ELSEIF (creditlim <= 50000 AND creditlim >= 10000) THEN
	        SET p_customerLevel = 'GOLD';
	    ELSEIF creditlim < 10000 THEN
	        SET p_customerLevel = 'SILVER';
	    END IF;
	 
	END$$

####### Example with CASE

	DELIMITER $$
 
	CREATE PROCEDURE GetCustomerShipping(
	 in  p_customerNumber int(11), 
	 out p_shiping        varchar(50))
	BEGIN
	    DECLARE customerCountry varchar(50);
	 
	    SELECT country INTO customerCountry
	 FROM customers
	 WHERE customerNumber = p_customerNumber;
	 
	    CASE customerCountry
	 WHEN  'USA' THEN
	    SET p_shiping = '2-day Shipping';
	 WHEN 'Canada' THEN
	    SET p_shiping = '3-day Shipping';
	 ELSE
	    SET p_shiping = '5-day Shipping';
	 END CASE;
	 
	END$$


########## Example with WHILE

LEAVE == BREAK
ITERATE == CONTINUE

 DELIMITER $$
 DROP PROCEDURE IF EXISTS test_mysql_while_loop$$
 CREATE PROCEDURE test_mysql_while_loop()
 BEGIN
 DECLARE x  INT;
 DECLARE str  VARCHAR(255);
 
 SET x = 1;
 SET str =  '';
 
 WHILE x  <= 5 DO
 SET  str = CONCAT(str,x,',');
 SET  x = x + 1; 
 END WHILE;
 
 SELECT str;
 END$$
DELIMITER ;

########## Example with REPEAT

 DELIMITER $$
 DROP PROCEDURE IF EXISTS mysql_test_repeat_loop$$
 CREATE PROCEDURE mysql_test_repeat_loop()
 BEGIN
 DECLARE x INT;
 DECLARE str VARCHAR(255);
        
 SET x = 1;
        SET str =  '';
        
 REPEAT
 SET  str = CONCAT(str,x,',');
 SET  x = x + 1; 
        UNTIL x  > 5
        END REPEAT;
 
        SELECT str;
 END$$
 DELIMITER ;


# syntax

CREATE
    [OR REPLACE]
    [DEFINER = { user | CURRENT_USER | role | CURRENT_ROLE }]
    PROCEDURE sp_name ([proc_parameter[,...]])
    [characteristic ...] routine_body

proc_parameter:
    [ IN | OUT | INOUT ] param_name type

type:
    Any valid MariaDB data type

characteristic:
    LANGUAGE SQL
  | [NOT] DETERMINISTIC
  | { CONTAINS SQL | NO SQL | READS SQL DATA | MODIFIES SQL DATA }
  | SQL SECURITY { DEFINER | INVOKER }
  | COMMENT 'string'

routine_body:
    Valid SQL procedure statement


# example1

DELIMITER //

CREATE PROCEDURE simpleproc (OUT param1 INT)
 BEGIN
  SELECT COUNT(*) INTO param1 FROM t;
 END;
//

DELIMITER ;

CALL simpleproc(@a);

SELECT @a;


# example2


##########################################################################################
#  WITH
##########################################################################################


WITH salesrep AS (
    SELECT 
        employeeNumber,
        CONCAT(firstName, ' ', lastName) AS salesrepName
    FROM
        employees
    WHERE
        jobTitle = 'Sales Rep'
),
customer_salesrep AS (
    SELECT 
        customerName, salesrepName
    FROM
        customers
            INNER JOIN
        salesrep ON employeeNumber = salesrepEmployeeNumber
)
SELECT 
    *
FROM
    customer_salesrep
ORDER BY customerName;


##########################################################################################
#  triggers
##########################################################################################

	
SHOW TRIGGERS;
SHOW TRIGGERS [FROM|IN] database_name [LIKE expr | WHERE expr];

DROP TRIGGER employees.before_employees_update;

# this doesn't activate any trigger
TRUNCATE `table`;

BEFORE INSERT – activated before data is inserted into the table.
AFTER INSERT – activated after data is inserted into the table.
BEFORE UPDATE – activated before data in the table is updated.
AFTER UPDATE – activated after data in the table is updated.
BEFORE DELETE – activated before data is removed from the table.
AFTER DELETE – activated after data is removed from the table.


INSESRT - NEW
DELETE  - OLD
UPDATE - OLD & NEW 


# complex trigger
DELIMITER $$
CREATE TRIGGER before_employee_update 
    BEFORE UPDATE ON employees
    FOR EACH ROW 
BEGIN
    INSERT INTO employees_audit
    SET action = 'update',
     employeeNumber = OLD.employeeNumber,
        lastname = OLD.lastname,
        changedat = NOW(); 
END$$
DELIMITER ;


# simple trigger
CREATE TRIGGER increment_animal 
AFTER INSERT ON animals 
FOR EACH ROW 
UPDATE animal_count SET animal_count.animals = animal_count.animals+1;



##########################################################################################
#  grant
##########################################################################################

# syntax
GRANT
    priv_type [(column_list)]
      [, priv_type [(column_list)]] ...
    ON [object_type] priv_level
    TO user  [IDENTIFIED [BY [PASSWORD] 'password']
        |{VIA|WITH} plugin_name 
           [{USING|AS} 'plugin_option']]
        [, user [IDENTIFIED [BY [PASSWORD] 'password']
        |{VIA|WITH} plugin_name] 
           [{USING|AS} 'plugin_option']]
    user_options...

GRANT PROXY ON user_specification
    TO user_specification [, user_specification] ...
    [WITH GRANT OPTION]

user_options:
    [REQUIRE {NONE | tls_option [[AND] tls_option] ...}]
    [WITH with_option [with_option] ...]

object_type:
    TABLE
  | FUNCTION
  | PROCEDURE

priv_level:
    *
  | *.*
  | db_name.*
  | db_name.tbl_name
  | tbl_name
  | db_name.routine_name

with_option:
    GRANT OPTION
  | MAX_QUERIES_PER_HOUR count
  | MAX_UPDATES_PER_HOUR count
  | MAX_CONNECTIONS_PER_HOUR count
  | MAX_USER_CONNECTIONS count
  | MAX_STATEMENT_TIME time

tls_option:
    SSL
  | X509
  | CIPHER 'cipher'
  | ISSUER 'issuer'
  | SUBJECT 'subject'


-- current user
SHOW GRANTS;
-- perticular user
SHOW GRANTS FOR user_name;
-- revoke all permissions
REVOKE ALL PRIVILEGES, GRANT OPTION FROM user_name;

-- The USAGE privilege grants no real privileges
-- it is used to other privileges without changing current privileges
-- like grant option
GRANT USAGE ON db.* TO USER WITH GRANT OPTION;

# example 1
GRANT ALL ON  *.* to 'alexander'@'localhost' WITH GRANT OPTION;

CREATE USER 'coordinator'@'localhost' IDENTIFIED BY 'abc';

CREATE ROLE 'reader';
GRANT SELECT ON University.* TO 'reader';
GRANT SELECT ON university.student TO "student"

GRANT 'reader' TO 'coordinator'@'localhost';

CREATE ROLE 'writer';
GRANT INSERT ON University.* TO 'writer';
SET ROLE 'raeder';

CREATE USER 'datamaster'@'localhost' IDENTIFIED BY 'abc';
GRANT 'writer' TO 'datamaster'@'localhost';

SET ROLE 'raeder';



CREATE USER 'moderator'@'localhost' IDENTIFIED BY 'abc';
GRANT 'reader' TO 'moderator'@'localhost';
GRANT 'writer' TO 'moderator'@'localhost';


##########################################################################################
#  users & roles
##########################################################################################

# user syntax

CREATE [OR REPLACE] USER [IF NOT EXISTS] 
 user_specification [,user_specification] ...
  [REQUIRE {NONE | tls_option [[AND] tls_option] ...}]
  [WITH resource_option [resource_option] ...]

user_specification:
  username [authentication_option]

authentication_option:
  IDENTIFIED BY 'authentication_string' 
  | IDENTIFIED BY PASSWORD 'hash_string'
  | IDENTIFIED {VIA|WITH} authentication_plugin
  | IDENTIFIED {VIA|WITH} authentication_plugin BY 'authentication_string'
  | IDENTIFIED {VIA|WITH} authentication_plugin {USING|AS} 'hash_string'

tls_option:
  SSL 
  | X509
  | CIPHER 'cipher'
  | ISSUER 'issuer'
  | SUBJECT 'subject'

resource_option:
  MAX_QUERIES_PER_HOUR count
  | MAX_UPDATE_PER_HOUR count
  | MAX_CONNECTIONS_PER_HOUR count
  | MAX_USER_CONNECTIONS count


# user example 1
CREATE USER foo2@test IDENTIFIED BY 'password';

# user example 2
CREATE USER foo 
  WITH MAX_QUERIES_PER_HOUR 10
  MAX_UPDATES_PER_HOUR 20
  MAX_CONNECTIONS_PER_HOUR 30
  MAX_USER_CONNECTIONS 40;




######### update Password
######
USE mysql;
 
UPDATE user 
SET password = PASSWORD('dolphin')
WHERE user = 'dbadmin' AND 
      host = 'localhost';
 
FLUSH PRIVILEGES;



######
SET PASSWORD FOR 'dbadmin'@'localhost' = PASSWORD('bigshark');
ALTER USER dbadmin@localhost IDENTIFIED BY 'littlewhale';


########## roles 

# create role, grant permissions and assign role to user
CREATE ROLE writer;
GRANT SELECT ON data.* TO writer;
GRANT writer TO journalist;


# current role
SELECT CURRENT_ROLE;


# set role
SET ROLE `rollname`;


##########################################################################################
#  Events
##########################################################################################


#status of events
SHOW VARIABLES WHERE VARIABLE_NAME = 'event_scheduler';
#toogle events
SET GLOBAL event_scheduler = ON;
# have a look at created events
SELECT * FROM mysql.event\G;
SHOW PROCESSLIST;
SHOW EVENTS FROM classicmodels;
DROP EVENT [IF EXIST] event_name;


#### alter events

ALTER EVENT test_event_04 ON SCHEDULE EVERY 2 MINUTE;

ALTER EVENT test_event_04
DO
   INSERT INTO messages(message,created_at)
   VALUES('Message from event',NOW());

ALTER EVENT test_event_04 
DISABLE;

#syntax
CREATE [OR REPLACE]
    [DEFINER = { user | CURRENT_USER | role | CURRENT_ROLE }]
    EVENT 
    [IF NOT EXISTS]
    event_name    
    ON SCHEDULE schedule
    [ON COMPLETION [NOT] PRESERVE]
    [ENABLE | DISABLE | DISABLE ON SLAVE]
    [COMMENT 'comment']
    DO sql_statement;

schedule:
    AT timestamp [+ INTERVAL interval] ...
  | EVERY interval 
    [STARTS timestamp [+ INTERVAL interval] ...] 
    [ENDS timestamp [+ INTERVAL interval] ...]

interval:
    quantity {YEAR | QUARTER | MONTH | DAY | HOUR | MINUTE |
              WEEK | SECOND | YEAR_MONTH | DAY_HOUR | DAY_MINUTE |
              DAY_SECOND | HOUR_MINUTE | HOUR_SECOND | MINUTE_SECOND}



#example
CREATE EVENT example ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 1 MINUTE DO UPDATE ;

CREATE EVENT example
ON SCHEDULE EVERY 1 HOUR
STARTS CURRENT_TIMESTAMP + INTERVAL 1 MONTH
ENDS CURRENT_TIMESTAMP + INTERVAL 1 MONTH + INTERVAL 1 WEEK
DO some_task;



##########################################################################################
#  Storage Engines
##########################################################################################



##########################################################################################
#  Locks
##########################################################################################
LOCK TABLES table_name [READ | WRITE]
UNLOCK TABLES;
SHOW PROCESSLIST;



##########################################################################################
#  variables
##########################################################################################

select @orderNumber := max(orderNUmber) from orders;
set @orderNumber = @orderNumber  + 1;



##########################################################################################
#  AutoCommit
##########################################################################################

SHOW VARIABLES WHERE VARIABLE_NAME = 'AUTOCOMMIT';
SET AUTOCOMMIT=0;
COMMIT;
ROLLBACK;



##########################################################################################
#  Profiling
##########################################################################################

SET profiling = 1;

SHOW PROFILE [type [, type] ... ]
    [FOR QUERY n]
    [LIMIT row_count [OFFSET offset]]

type:
    ALL
  | BLOCK IO
  | CONTEXT SWITCHES
  | CPU
  | IPC
  | MEMORY
  | PAGE FAULTS
  | SOURCE
  | SWAPS

SHOW PROFILE FOR QUERY 4;
SHOW PROFILE CPU FOR QUERY 5;



##########################################################################################
#  index
##########################################################################################

	
SHOW INDEX FROM db.table;

CREATE [UNIQUE|FULLTEXT|SPATIAL] INDEX index_name
USING [BTREE | HASH | RTREE] 
ON table_name (column_name [(length)] [ASC | DESC],...)


CREATE INDEX officeCode ON employees(officeCode);
DROP INDEX index_name ON table_name;


##########################################################################################
#  tables
##########################################################################################


# create table and insert data on the fly
CREATE TABLE inst_dept AS query;
# create table with schema of b table
create table a like b;
# reset the table, reset auto_increment value to 0
TRUNCATE TABLE table_name; 

ALTER TABLE tasks CHANGE COLUMN task_id task_id INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE tasks ADD COLUMN complete DECIMAL(2,1) NULL AFTER description;
ALTER TABLE tasks DROP COLUMN description;
ALTER TABLE tasks RENAME TO work_items;
# rename mutiple table
RENAME TABLE old_table_name_1 TO new_table_name_2,
             old_table_name_2 TO new_table_name_2,...


##########################################################################################
#  functions
##########################################################################################

# inbuilt functions
IFNULL(col, 0) # return 0 if null else return value
CURDATE()
CURTIME()


for the same input parameters, if the stored function returns the same result, 
it is considered deterministic and otherwise the stored function is not deterministic. 
You have to decide whether a stored function is deterministic or not. If you declare it incorrectly,
the stored function may produce an unexpected result,
or the available optimization is not used which degrades the performance.

###### Example

DELIMITER $$
 
CREATE FUNCTION CustomerLevel(p_creditLimit double) RETURNS VARCHAR(10)
    DETERMINISTIC
BEGIN
    DECLARE lvl varchar(10);
 
    IF p_creditLimit > 50000 THEN
 SET lvl = 'PLATINUM';
    ELSEIF (p_creditLimit <= 50000 AND p_creditLimit >= 10000) THEN
        SET lvl = 'GOLD';
    ELSEIF p_creditLimit < 10000 THEN
        SET lvl = 'SILVER';
    END IF;
 
 RETURN (lvl);
END


######## user defined functions

CREATE FUNCTION max_salary (dept TINYTEXT) RETURNS INT RETURN
  (SELECT MAX(salary) FROM employees WHERE employees.dept = dept);


# list declared fucntions
SHOW FUNCTION STATUS\G;



##########################################################################################
#  Operators
##########################################################################################

=
<>
AND
OR
NOT

EXISTS

    SELECT column_name(s)
    FROM table_name
    WHERE EXISTS
    (SELECT column_name FROM table_name WHERE condition); 

ANY
    value > ANY (1,2,3,4)

ALL
    value > ALL (1,2,3,4)

# begin and end values are included 
BETWEEN v1 AND v2 | NOT BETWEEN v1 AND v2

BETWEEN CAST('2003-01-01' AS DATE)
        AND CAST('2003-01-31' AS DATE);	

SELECT * FROM Orders
WHERE OrderDate BETWEEN #07/04/1996# AND #07/09/1996#;

LIKE | NOT LIKE

IN | NOT IN

IS NULL | IS NOT NULL

JOIN
    
    (INNER) JOIN: Returns records that have matching values in both tables
    LEFT (OUTER) JOIN: Return all records from the left table, and the matched records from the right table
    RIGHT (OUTER) JOIN: Return all records from the right table, and the matched records from the left table
    FULL (OUTER) JOIN: Return all records when there is a match in either left or right table



##########################################################################################
#  Some Syntax
##########################################################################################


# sorting
ORDER BY column1, column2, ... ASC|DESC;  default ASC

# LIKE 

    % - The percent sign represents zero, one, or multiple characters
    _ - The underscore represents a single character

    WHERE City LIKE '[bsp]%';
    WHERE City LIKE '[a-c]%';
    WHERE City LIKE '[!bsp]%';
    WHERE City NOT LIKE '[bsp]%'; 

Create constraint constName .... initially defered; 

Create constraint constName .... deferrable; 

Set constraint `constarintList` deferrered; 



##########################################################################################
#   Dangerous Examples
##########################################################################################

######## nth entry in the sorted order
###### concept : limit offset, count
SELECT 
    column1, column2,...
FROM
    table
ORDER BY column1 DESC
LIMIT nth-1, count;


######## nth entry in the sorted order
###### concept : limit offset, count




##########################################################################################
#   DataType
##########################################################################################

TIME : 'HH:MM:SS'
	083000 is alllowed, without semicolon

Date : yyyy-mm-dd , DATE_FORMAT() to change the format
	CURDATE()


DATETIME : YYYY-MM-DD HH:MM:SS
	NOW()
	DATE(expr)   - extracts DATE from expr
	HOUR(@dt),
    MINUTE(@dt),
    SECOND(@dt),
    DAY(@dt),
    WEEK(@dt),
    MONTH(@dt),
    QUARTER(@dt),
    YEAR(@dt);
	
	# cal future dateTime
   DATE_ADD(@dt, INTERVAL 1 SECOND) '1 second later',
   DATE_ADD(@dt, INTERVAL 1 MINUTE) '1 minute later',
   DATE_ADD(@dt, INTERVAL 1 HOUR) '1 hour later',
   DATE_ADD(@dt, INTERVAL 1 DAY) '1 day later',
   DATE_ADD(@dt, INTERVAL 1 WEEK) '1 week later',
   DATE_ADD(@dt, INTERVAL 1 MONTH) '1 month later',
   DATE_ADD(@dt, INTERVAL 1 YEAR) '1 year later';

   # cal past time
   DATE_SUB(@dt, INTERVAL 1 unit)

   DATE_DIFF(@dt1, @dt2)

TIMESTAMP : stores in UTC time, so depending on the timeZone output changes

DECIMAL(6,2)    range - 9999.99 to -9999.99

ENUM(A,B,C,...)     can use 1,2,3,... for insert

BIT(n)
