###############################################################
C)

1)

CREATE USER 'coordinator'@'localhost' IDENTIFIED BY 'abc';

2)

CREATE ROLE 'reader';
GRANT SELECT ON University.* TO 'reader';


3)

GRANT 'reader' TO 'coordinator'@'localhost';

4)

CREATE ROLE 'writer';
GRANT INSERT ON University.* TO 'writer';
SET ROLE 'raeder';

5)

CREATE USER 'datamaster'@'localhost' IDENTIFIED BY 'abc';
GRANT 'writer' TO 'datamaster'@'localhost';

SET ROLE 'raeder';


6)


CREATE USER 'moderator'@'localhost' IDENTIFIED BY 'abc';
GRANT 'raeder' TO 'moderator'@'localhost';
GRANT 'writer' TO 'moderator'@'localhost';



###############################################################

D)



1)

DELIMITER //

CREATE PROCEDURE stud_to_cred ( OUT avg_credit DECIMAL(3,0))
BEGIN
	SELECT AVG(tot_cred) INTO avg_credit FROM University.student;
END;

//

DELIMITER ;

CALL stud_to_cred (@avg_credit);

SELECT @avg_credit;



SELECT ID, name from student where tot_cred >= 2 * @avg_credit;


#update student as S,(select ID,sum(ifnull(credits,0)) as GO from takes as T, course as C where T.course_id=C.course_id and ifnull(grade,'F') <> 'F' group by ID) as O set S.tot_cred = O.GO where S.ID=O.ID;

2)

CREATE FUNCTION fun_sel_avg (threshold DECIMAL(12,2)) RETURNS DECIMAL(12,2) RETURN
  (SELECT avg(budget) FROM department WHERE department.budget > threshold);

SELECT fun_sel_avg(10000) FROM department;


3)

DELIMITER //

CREATE PROCEDURE sp_top_avg ( IN N INT, OUT avg_budget DECIMAL(12,2))
BEGIN
	SELECT AVG(budget) INTO avg_budget FROM (SELECT * FROM University.department ORDER BY budget DESC LIMIT N) AS tmp;
END;

//

DELIMITER ;

CALL sp_top_avg (3, @avg_budget);

SELECT @avg_budget;


4)

insert into department value ('Dance','Watson',10000);


DELIMITER //
CREATE PROCEDURE update_budget ()
BEGIN
DECLARE tmp DECIMAL(12,2);
SET tmp = (SELECT AVG(budget) FROM University.department);
UPDATE University.department
SET department.budget = (
CASE 
WHEN (tmp/2) < (department.budget/2) THEN (tmp/2)
ELSE department.budget
END );
END;
//
DELIMITER ;
CALL update_budget ();



E)


1)

CREATE TABLE menu (
id int primary key,
name varchar(50) not null,
type enum('healthy','unhealthy')
);

CREATE TABLE `order` (
id int primary key,
count int not null
);

CREATE TABLE price (
id int primary key,
amount float not null,
constraint fk foreign key (id) references menu(id)
);


2)

Create a trigger ​ init ​ that will initiate the ​ count ​ to zero for each entry in the ​ menu ​ table.
Insert one record to menu table, and show how ​ init ​ works.

CREATE TRIGGER init AFTER INSERT ON `order`
FOR EACH ROW
BEGIN
count = 0;
END