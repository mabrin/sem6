import matplotlib.pyplot as plt
import numpy as np




def gauss(mean  , cov=[[[1,0],[0,1]],[[1,0],[0,1]]] , n=100):
    x, y = np.random.multivariate_normal(mean[0], cov[0] , n).T
                                    
    plt.plot(x, y, 'x')
    x, y = np.random.multivariate_normal(mean[1], cov[1] , n).T
    plt.plot(x, y, 'o')
    plt.axis('equal')
    plt.show()

m1=[0,0]
for i in range(5):
    D = 300* np.random.rand()
    print
    m2 = [D* np.math.cos(6.14 *np.random.rand() ) ,D* np.math.sin(6.14  *np.random.rand()) ]
  
    rand =np.random.randint(1000, size=4)
    S1 =[[rand[0],0],[0,rand[1]]]
    S2 =[[rand[2],0],[0,rand[3]]]
    
    cov = [S1 ,S2]
    gauss([m1,m2] ,cov )
    

