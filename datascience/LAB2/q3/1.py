from sklearn.naive_bayes import GaussianNB
import numpy as np
from sklearn.model_selection import train_test_split

data = np.genfromtxt('car.data' ,delimiter="," , autostrip = True )

X = data[:,:-1]
y = data[:,-1]
for i in range(5):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)
    clf = GaussianNB()
    clf.fit(X_train, y_train)
    print (str(i+1)+' vaidation: '+str((clf.score(X_test,y_test))))
