

import math
import matplotlib.pyplot as plt
import numpy as np
 

out=[]

def tea(flag ,prior,a,b ,out):
    
    p =np.random.beta(a,b)
    
     
    if(p >.5):
        out.append(1)
         
    else:
        out.append(0)
         
        
    
    return [p,a+flag ,b+1-flag]
     

prior = tea(1,.5,2,2,out)

input=[0,0,1,1,1,1,1,1,1,1 ,1,1,1,1,1,1,1,1,1]

for i in input:
    prior = tea(i,prior[0],prior[1],prior[2],out)
input.append(1)
x = [i for i in range(len(input))]   
plt.plot(x, input,'b-',label="actual")
plt.plot(x, out ,'ro',label="predicted" )
plt.legend(loc= 'lower right')
plt.show()

