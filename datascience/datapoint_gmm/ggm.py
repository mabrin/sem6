
import numpy as np
import matplotlib.pyplot as plt

'''-----------------------important ----------------------'''
def gausian(mean = [0, 0],cov = [[1, 0], [0, 1]],n=1):
    x, y = np.random.multivariate_normal(mean, cov, n).T
    return  (x ,y)

def set_covariance_cluster(x):
    if x==0 :
        return [[2,0],[0,3]] ##igher covariance
    else :
        return [[1,0],[0,1]] ##lower covariance

n=4
s = np.random.dirichlet((50, 10, 10 ,10), 1).transpose()
def sample_Z(theta ):
    zi = np.random.rand(1,1)
    x= theta[0]
    for i in range(len(theta)):
        if zi < x :
            return i ;
        else :
            x = x +theta[i+1]
    return len(theta) -1

    
cl1 = [[],[],[],[],[],[]]
cl2 = [[],[],[],[],[],[]]

cv1 = [ [[40,0],[0,40]],[[8,0],[0,8]]]

m1 ,m2 = gausian(cov = cv1[0],n=2)

x,y =gausian([m1[0],m2[0]])
z1 ,z2 = gausian([m1[1],m2[1]],cv1[1],n)

x = np.append(x,z1)
y = np.append(y,z2)

for j in range(2000):
    i=sample_Z(s)
    x1 ,y1 = np.random.multivariate_normal([x[i],y[i]],set_covariance_cluster(i), 1).T
    
    cl1[i].append( x1[0])
    cl2[i].append( y1[0])
    
    
plt.plot(cl1[0], cl2[0], 'x')
plt.plot(cl1[1], cl2[1], 'x')
plt.plot(cl1[2], cl2[2], 'x')
plt.plot(cl1[3], cl2[3], 'x')
plt.plot(cl1[4], cl2[4], 'x')
plt.plot(cl1[5], cl2[5], 'x')

plt.axis('equal')
plt.show()